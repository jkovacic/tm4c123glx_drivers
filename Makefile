# Copyright 2014, Jernej Kovacic
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Version 2013-05.23 of the Sourcery toolchain is used as a build tool.
# See comments in "setenv.sh" for more details about downloading it
# and setting the appropriate environment variables.

#
# Type "make help" for more details.
#


TOOLCHAIN = arm-none-eabi-
CC = $(TOOLCHAIN)gcc
CXX = $(TOOLCHAIN)g++
AS = $(TOOLCHAIN)as
LD = $(TOOLCHAIN)ld
OBJCPY = $(TOOLCHAIN)objcopy
AR = $(TOOLCHAIN)ar

CPUFLAG = -mthumb -mcpu=cortex-m4
WFLAG = -Wall -Wextra -Werror
FPUFLAG=-mfpu=fpv4-sp-d16 -mfloat-abi=softfp

CFLAGS = $(CPUFLAG) $(WFLAG)
# Uncomment this if the application performs floating point operations
#CFLAGS += $(FPUFLAG)

# Additional C compiler flags to produce debugging symbols
DEB_FLAG = -g -DDEBUG

OBJS = handlers.o startup.o sysctl.o systick.o nvic.o interrupt.o \
       scb.o gpio.o uart.o watchdog.o led.o switch.o main.o

# Uncomment this if the aplication uses the FPU
#OBJS += fpu.o

BSP_DEP = bsp.h
DEFAULT_DEP = app_defaults.h
LINKER_SCRIPT = tiva.ld
ELF_IMAGE = tiva.elf
IMAGE = tiva.bin



#
# Targets:
#

all : $(IMAGE)

rebuild : clean all

$(IMAGE) : $(ELF_IMAGE)
	$(OBJCPY) -O binary $< $@

$(ELF_IMAGE) : $(OBJS) $(LINKER_SCRIPT)
	$(LD) -nostdlib -T $(LINKER_SCRIPT) $(OBJS) -o $@

debug : _debug_flags all

debug_rebuild : _debug_flags rebuild

_debug_flags :
	$(eval CFLAGS += $(DEB_FLAG))

handlers.o : handlers.c
	$(CC) -c $(CFLAGS) $< -o $@
	
startup.o : startup.c $(DEFAULT_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

sysctl.o : sysctl.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

systick.o : systick.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

nvic.o : nvic.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

interrupt.o : interrupt.c
	$(CC) -c $(CFLAGS) $< -o $@
	
scb.o : scb.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

fpu.o : fpu.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

gpio.o : gpio.c $(BSP_DEP) $(DEFAULT_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

uart.o : uart.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

watchdog.o : watchdog.c $(BSP_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

led.o : led.c
	$(CC) -c $(CFLAGS) $< -o $@

switch.o : switch.c
	$(CC) -c $(CFLAGS) $< -o $@	

main.o : main.c $(DEFAULT_DEP)
	$(CC) -c $(CFLAGS) $< -o $@

# Cleanup directives:
clean_obj :
	$(RM) *.o

clean_intermediate : clean_obj
	$(RM) *.elf
	$(RM) *.img

clean : clean_intermediate
	$(RM) *.bin

# Short help instructions:
help:
	@echo
	@echo Valid targets:
	@echo - all: builds missing dependencies and creates the target image \'$(IMAGE)\'.
	@echo - rebuild: rebuilds all dependencies and creates the target image \'$(IMAGE)\'.
	@echo - debug: same as \'all\', also includes debugging symbols to \'$(ELF_IMAGE)\'.
	@echo - debug_rebuild: same as \'rebuild\', also includes debugging symbols to \'$(ELF_IMAGE)\'. 
	@echo - clean_obj: deletes all object files, only keeps \'$(ELF_IMAGE)\' and \'$(IMAGE)\'.
	@echo - clean_intermediate: deletes all intermediate binaries, only keeps the target image \'$(IMAGE)\'.
	@echo - clean: deletes all intermediate binaries, incl. the target image \'$(IMAGE)\'.
	@echo - help: displays these help instructions.
	@echo

.PHONY : all rebuild clean clean_intermediate clean_obj debug debug_rebuild _debug_flags help
