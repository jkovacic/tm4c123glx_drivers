/*
Copyright 2014, Jernej Kovacic

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * @file
 *
 * A simple demo application, designed with a purpose to
 * unit test implemented device drivers (Sysctl, SysTick,
 * NVIC, UART, watchdog, GPIO, etc.).
 *
 * INSTRUCTIONS:
 *
 * After the application starts, it waits until any of
 * both switches is pressed.
 *
 * When this occurs, a simple light show starts: a combination
 * of LEDs is turned on for 1 second, then it is replaced by
 * another combination for another 1 second and so on.
 *
 * The switch 2 must be pressed regularly. If it has not been
 * pressed for 10 seconds, a watchdog will reset the application.
 *
 * By pressing switch 1, the light show either pauses (the last
 * combination of LEDs remains turned on) or resumes. Even when
 * the light show is paused, the watchdog remains active, so the
 * switch 2 must still be pressed regularly in order to prevent
 * a reset.
 *
 * Additional messages are also displayed by the UART0,
 * set to 115200 bps, 8 data bits, no parity, 1 stop bit.
 * It is connected to the debug USB port.
 *
 * @author Jernej Kovacic
 */


#include <stdint.h>
#include <stdbool.h>

#include "app_defaults.h"
#include "sysctl.h"
#include "uart.h"
#include "led.h"
#include "switch.h"
#include "systick.h"
#include "gpio.h"
#include "interrupt.h"
#include "watchdog.h"


/*
 * This diagnostic pragma will suppress the -Wmain warning,
 * raised when main() does not return an int
 * (which is perfectly OK in bare metal programming!).
 *
 * More details about the GCC diagnostic pragmas:
 * https://gcc.gnu.org/onlinedocs/gcc/Diagnostic-Pragmas.html
 */
#pragma GCC diagnostic ignored "-Wmain"


/*
 * Two global variables for communication between ISR
 * handlers and the main application's loop.
 * - 'systick_counter' counts the number of systick's ticks (1 ms)
 * - 'sw1pressed' indicates that the switch 1 has been pressed.
 */
static volatile uint32_t systick_counter;
static volatile bool sw1pressed;


/* Duration of 1 light show state in milliseconds */
#define PAUSE_MS            ( 1000 )


/*
 * A simple delay function, only applicable when the SysTick
 * interrupt triggering is disabled.
 *
 * @param ms - delay time in milliseconds
 */
void delay(uint32_t ms)
{
    /* counter of millisecond ticks */
    uint32_t cnt = 0;

    /* start the SysTick and reload its counter: */
    systick_clear();
    systick_enable();

    /* wait until the count event has been sensed 'ms' times */
    for ( cnt=0; cnt<ms; ++cnt )
    {
        while ( false == systick_countSet() );
    }

    /* and stop the SysTick */
    systick_disable();
}


/*
 * SysTick interrupt handler.
 *
 * It only increments the counter variable
 * that is processed by the main application loop.
 */
__attribute__ ((interrupt))
void SysTickIntHandler(void)
{
    /* just increment the counter of ticks: */
    ++systick_counter;
    /* no need to acknowledge (clear) the SysTick interrupt */
}


/*
 * Switch 1 interrupt handler.
 *
 * It only sets a "global" variable that is further
 * processed by the main application loop.
 */
static void sw1handler(void)
{
    uint32_t i;

    sw1pressed = true;

    /*
     * A short delay, it prevents the same switch press
     * to be "detected" multiple times.
     */
    for ( i=0; i<10*DEF_SYSTICK_RELOAD; ++i);
    /* TODO: use another timer, when a driver is implemented */

    switch_clearIntr(1);
}


/*
 * Switch 2 interrupt handler.
 *
 * It reloads the watchdog timer.
 */
static void sw2handler(void)
{
    wd_reload(DEF_WD_NR);
    switch_clearIntr(2);
}


/*
 * Selected watchdog interrupt handler.
 *
 * It resets the board.
 */
static void wdHandler(void)
{
    intr_reset();

    /* The interrupt flag is intentionally not cleared. */
}


/*
 * Main function, entered from ReswetISR, implemented in startup.c.
 *
 * It configures all supported peripherals and start the main
 * application loop.
 *
 * Although this is also handled elsewhere, one should be aware
 * that the function is supposed to never return.
 */
void main(void)
{
    uint8_t state;     /* current state of the light show */
    bool systick_enabled;

    /* Combinations of LEDs for each state: */
    const uint8_t states[7] =
    {
        LED_RED,
        LED_RED | LED_GREEN,
        LED_GREEN,
        LED_GREEN | LED_BLUE,
        LED_BLUE,
        LED_BLUE | LED_RED,
        LED_RED | LED_BLUE | LED_GREEN
    };


    /* Configure system clock frequency to 50 MHz (default) */
    sysctl_configSysClock(DEF_SYS_CLOCK_DIV);

    /* Configure the SysTick to reload every 1 ms */
    systick_config(DEF_SYSTICK_RELOAD);

    /* Priority of SysTick's interrupts */
    systick_setPriority(1);

    /* Priority of watchdog's interrupts */
    wd_setIntPriority(0);

    /* Configure LED' and switches' GPIO pins: */
    led_config();
    switch_config();

    /* And configure and enable the UART 0: */
    uart_config(
            0,
            DEF_UART0_PORT,
            DEF_UART0_PIN_RX,
            DEF_UART0_PIN_TX,
            DEF_UART0_PCTL,
            DEF_UART0_BR,
            DEF_UART0_DATA_BITS,
            DEF_UART0_PARITY,
            DEF_UART0_STOP );

    uart_disableRx(0);
    uart_enableTx(0);
    uart_enableUart(0);


    /* Enable all interrupts at the PRIMASK system register: */
    intr_enableInterrupts();

    /* Register interrupt handlers for both switches: */
    switch_registerIntrHandler(1, &sw1handler);
    switch_registerIntrHandler(2, &sw2handler);

    uart_printStr(0, "Application started!\r\n");


    /*
     * During the first stage, the switches' statuses
     * will be polled, so their interrupt triggering
     * will be disabled.
     */
    switch_disableSwInt(1);
    switch_disableSwInt(2);

    /* SysTick interrupts are temporarily also disabled */
    systick_disableInterrupt();

    /*
     * Reset LEDs' GPIO pins, disable the watchdog and
     * configure it to reset after 10 seconds:
     */
    led_allOff();
    wd_reset(DEF_WD_NR);
    wd_config(DEF_WD_NR, DEF_WD_LOAD, WDEX_IRQ_RESET);
    wd_registerIntHandler(DEF_WD_NR, &wdHandler);

    /* Transmit an instruction to the UART: */
    uart_printStr(0, "Press any switch to start\r\n");

    delay(500);

    /* wait until any switch is pressed */
    while ( 0 == switch_statusBoth() );

    /*
     * To prevent immediate pausing of the light show,
     * wait until at least switch 1 is no longer pressed
     * and then proceed to the next stage.
     */
    while ( 0 != switch_statusSw1() );

    /*
     * The second stage of the application starts here.
     * Internal variables for communication with ISR handlers
     * are initialized, SysTick's interrupts are enabled
     * and it is started, the watchdog is also started,
     * another instruction is transmitted to the UART.
     */

    sw1pressed = false;
    systick_counter = 0;

    systick_enableInterrupt();
    systick_enabled = true;
    systick_enable();

    wd_start(DEF_WD_NR);

    state = 0;
    uart_printStr(0, "Light show started\r\n");

    /* Enable switches' interrupts */
    switch_clearIntr(1);
    switch_clearIntr(2);
    switch_enableSwInt(1);
    switch_enableSwInt(2);

    /*
     * This loop occurs every 1 second and performs
     * transition of the light show state.
     */
    for ( ; ; )
    {
        /*
         * Turn off all LEDs and turn on the
         * combination, depending on state.
         */
        led_allOff();
        led_on(states[state]);

        /* increment the state variable */
        state++;
        state %= 7;

        /*
         * Wait until any interrupt request occurs.
         */
        while ( systick_counter < PAUSE_MS )
        {
            /* if sw1 interrupt occurred, start or stop the SysTick */
            if ( true == sw1pressed )
            {
                if ( false == systick_enabled )
                {
                    /* enable SysTick: */
                    systick_enable();
                    systick_enabled = true;
                    uart_printStr(0, "Light show resumed.\r\n");
                }
                else
                {
                    /* disable SysTick: */
                    systick_disable();
                    systick_enabled = false;
                    uart_printStr(0, "Light show paused, press SW1 to resume.\r\n");
                }

                /* reset the sw1 indicator */
                sw1pressed = false;
            }
        }



        /*
         * Reset the counter of ticks:
         */
        systick_counter = 0;
    } /* transition of states */


    /* An infinite loop, just in case... */
    for ( ; ; );
}
