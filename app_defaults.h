/*
Copyright 2014, Jernej Kovacic

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * @file
 *
 * Sensible defaults for supported peripherals of
 * Texas Instruments TM4C123GLX Launchpad.
 *
 * @author Jernej Kovacic
 */


#ifndef _APP_DEFAULTS_H_
#define _APP_DEFAULTS_H_

#include "pll_freq_divisors.h"
#include "uart.h"


/* By default set system clock frequency to 50 MHz: */
#define DEF_SYS_CLOCK_DIV       ( DIV_FREQ_50_MHZ )

/* Main stack size in 32-bit words: */
#define DEF_MAIN_STACK_SIZE_WORDS     ( 128 )

/* Process stack size in 32-bit words: */
#define DEF_PROCESS_STACK_SIZE_WORDS  ( 128 )

/* 
 * Enable the Floating Point Unit and stacking of its
 * register on interrupt events.
 */
#define DEF_FPU_ENABLE                0

/*
 * Will GPIO registers be accessed via the Advanced High-Performance
 * Bus (AHB)?
 * */
#define DEF_GPIO_AHB                  1

/*
 * If system clock frequency is set to 50 MHz,
 * the sensible default for a 1 ms tick is
 * 0.001 s * 50,000,000 Hz = 50,000
 */
#define DEF_SYSTICK_RELOAD      ( 50000 - 1 )


/*
 * Sensible defaults for all UART communications:
 * - 8 data bits
 * - no parity
 * - 1 stop bit
 */
#define DEF_UART_DATA_BITS      ( 8 )
#define DEF_UART_PARITY         ( PAR_NONE )
#define DEF_UART_STOP           ( 1 )


/*
 * Default settings for the UART0:
 * pins 0 and 1 of the GPIO port A, baud rate=115200
 */
#define DEF_UART0_PORT          ( GPIO_PORTA )
#define DEF_UART0_PIN_RX        ( 0 )
#define DEF_UART0_PIN_TX        ( 1 )
#define DEF_UART0_PCTL          ( 1 )
#define DEF_UART0_BR            ( BR_115200 )
#define DEF_UART0_DATA_BITS     DEF_UART_DATA_BITS
#define DEF_UART0_PARITY        DEF_UART_PARITY
#define DEF_UART0_STOP          DEF_UART_STOP

/*
 * Default settings for the UART1:
 * pins 0 and 1 of the GPIO port B, baud rate=115200
 */
#define DEF_UART1_PORT          ( GPIO_PORTB )
#define DEF_UART1_PIN_RX        ( 0 )
#define DEF_UART1_PIN_TX        ( 1 )
#define DEF_UART1_PCTL          ( 1 )
#define DEF_UART1_BR            ( BR_115200 )
#define DEF_UART1_DATA_BITS     DEF_UART_DATA_BITS
#define DEF_UART1_PARITY        DEF_UART_PARITY
#define DEF_UART1_STOP          DEF_UART_STOP


/* The selected watchdog and its settings */
#define DEF_WD_NR               ( 1 )
#define DEF_WD_LOAD             ( 160000000 )


#endif  /* _APP_DEFAULTS_H_ */
