## About
A simple collection of drivers and test cases for the 
[TI TM4C123GLX Launchpad](http://www.ti.com/tool/ek-tm4c123gxl), 
based on the [TI TM4C123GH6PM](http://www.ti.com/lit/ds/symlink/tm4c123gh6pm.pdf) CPU.
The design is modular enough (e.g. some board specific data are separated from drivers and
test cases) to simplify porting to other boards with similar hardware.
The project is experimental and its sole purpose is to provide necessary code that would facilitate 
porting [FreeRTOS](http://www.freertos.org/) to this board and possibly other similar 
ARM Cortex-M4 based boards.

At the moment only drivers for directly accessible peripherals are
available (SysTick, NVIC, UART, watchdogs, LEDs, switches). This means
that purchase of any additional hardware is not necessary.

The provided Makefile script will prepare an image, suitable to upload it
directly to the Launchpad and run it. 

## Prerequisites
* _Tiva&#x2122; C series TM4C123GLX Launchpad_
* A _Micro-B USB cable_, typically shipped with a Launchpad
* _Sourcery CodeBench Lite Edition for ARM EABI_ toolchain (now owned by Mentor Graphics), 
based on GCC. See comments in _setenv.sh_ for more details about downloading and installation.
* _GNU Make_
* _[LM4Tools](https://github.com/utzig/lm4tools)_ or 
_[TI LMFlash Programmer](http://www.ti.com/tool/lmflashprogrammer)_ 
to upload the image to the Launchpad
* Optionally _[OpenOCD](http://openocd.sourceforge.net/)_ for deugging.
See comments in _start\_openocd.sh_ for more details about installation.

## Build
A convenience Bash script _setenv.sh_ is provided to set paths to toolchain's commands 
and libraries. You may edit it and adjust the paths according to your setup. To set up 
the necessary paths, simply type:

`. ./setenv.sh`

To build the image with the test application, just run _make_ or _make rebuild_. 
If the build process is successful, the image file _tiva.bin_ will be ready to boot.

## Run
When the image _tiva.bin_ is successfully built, you may upload it to
the Launchpad, using the simple cross platform CLI tool 
[LM4Tools](https://github.com/utzig/lm4tools):

`/path/to/lm4flash tiva.bin`

Alternatively you may use the GUI tool 
[TI LMFlash Programmer](http://www.ti.com/tool/lmflashprogrammer), provided
by Texas Instruments. It is available for Windows only.

## Application
The test application is very simple. When started, it waits, until any switch
is pressed. Then a simple LED light show starts, turning on a different
combination of colors every second. 

The switch 2 must be pressed regularly otherwise a watchdog will reset 
the board after 10 seconds.

By pressing switch 1, the light show either pauses (the last active
combination of LEDs will remain turned on) or resumes. Note that even 
when the light show is paused, the watchdog remains active, so the
switch 2 must still be pressed regularly in order to prevent a reset.

Some basic information about the application states are also transmitted 
via UART0 and the Micro-B USB cable. The messages are then displayed
in a serial terminal such as _minicom_, _gtkterm_, etc.
The terminal must be configured for 115200 bps, 8 bits, no parity, 1 stop bit. 
A FTDI driver is required for a UART connection.

## License
The source code is licenced under the Apache 2.0 license. See LICENSE.txt and 
[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0) 
for more details.

## Author
The author of the application is Jernej Kova&#x010d;i&#x010d;.

